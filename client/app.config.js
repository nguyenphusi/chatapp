angular.
module('chatApp').
config(['$locationProvider', '$routeProvider',
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider
    .when('/', {
      template: '<register></register>'
    })
    .when('/chat/', {
      template: '<chat></chat>'
    })
    .otherwise('/');
  }
  ]);