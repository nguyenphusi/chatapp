angular.module('chatApp', [
	'ngRoute'
	,'luegg.directives'
	,'angular-web-notification',
	,'register'
	,'chat'
	,'userList',
	,'privateChat'
	]);
