angular.
module('chatApp').
component('userList', {
	templateUrl: '/client/user-list/user-list.template.html',
	controller: userListController
});

function userListController($http, $scope, $rootScope, socket, webNotification) {
	let vm = this;

	vm.openChat = (toUser) => {
		vm.toUser = toUser;
		$rootScope.$emit('open-chat', toUser);

	}

	// get list of connected users
	$http.get("/users/").then(
		(response)=>{
					//success callback
					console.log(response);
					// set list of current user
					vm.users = response.data.users;
					//remove current user from list
					delete vm.users[JSON.parse(sessionStorage.user).username];
				},
				(response)=>{
					//error callback
					console.log(response);
				});
	// add new connected user to list
	socket.on('add-user-list', (user)=>{
		// add new user to list
		$scope.$apply(() => {
			vm.users[user.username] = user; 

			// update current receiver socket id in case he/she reconnects
			if(typeof vm.toUser !== 'undefined' && vm.toUser.username == user.username){
				console.log('updated new current toUser id');
				vm.toUser = user;
			}
			console.log('added to user list:', user);
		});

		// show notification
		webNotification.showNotification(user.username + ' is online.', {
                onClick: function onNotificationClicked() {
                    console.log('Notification clicked.');
                    // open chat box with this user
                    vm.openChat(user);
                },
                autoClose: 4000
            }, onShow);


	});

	// remove user form user list when he/she disconnects
	socket.on('remove-user-list', (username)=>{
		// remove user form list
		$scope.$apply(() => {
			delete vm.users[username]; 
			console.log('removed user ',username);
		});

		// show notification
		webNotification.showNotification(username + ' is offline.', {
                onClick: function onNotificationClicked() {
                    console.log('Notification clicked.');
                },
                autoClose: 4000
            }, onShow);
	});

	// display new message in user list
	socket.on('receive-private-message', (privateMessage)=>{
		console.log('received private message in user-list: ', privateMessage);
		
		// display new message
		$scope.$apply(() => {
			vm.users[privateMessage.from.username].newMessage = privateMessage.message;
		});
		
		// show notification
		webNotification.showNotification(privateMessage.from.username + ' sent you a message.', {
                body: privateMessage.message,
                onClick: function onNotificationClicked() {
                    console.log('Notification clicked.');
                },
                autoClose: 4000
            }, onShow);

	});
	
	// onShow notification
	function onShow(error, hide) {
                if (error) {
                    console.log('Unable to show notification: ' + error.message);
                } else {
                    console.log('Notification Shown.');

                    setTimeout(function hideNotification() {
                        console.log('Hiding notification....');
                        hide(); //manually close the notification (you can skip this if you use the autoClose option)
                    }, 5000);
                }
            }

}