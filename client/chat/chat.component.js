angular.
module('chatApp').
component('chat', {
	templateUrl: '/client/chat/chat.template.html',
	controller: chatController
});

function chatController(socket, $location) {

	let vm = this;

	
	// reconnect socket in case user refresh page
	if(socket.disconnected){
		socket.connect();
	}

	// function logout
	vm.logout = () => {
		// disconnect socket
		socket.disconnect();
		// clear session
		sessionStorage.clear();
		// redirect to register page
		$location.path('/');
	}

	// on socket connect
	socket.on('connect', ()=>{
		console.log('Socket connected: ', socket.id);

		// store socket id in session
		sessionStorage.socketId = socket.id;
		vm.user = JSON.parse(sessionStorage.user);
		// set socket id to current user
		vm.user.id = sessionStorage.socketId;
		// update session
		sessionStorage.user = JSON.stringify(vm.user);

		console.log('new user: ', vm.user);

		// 
		socket.emit('new-user', vm.user);
	});
		
	}