angular.
module('chatApp').
component('privateChat', {
	templateUrl: '/client/private-chat/private-chat.template.html',
	controller: privateChatController
});

function privateChatController($rootScope, $scope, socket, $http, $timeout) {

	let vm = this;

	// message list between current selected receiver
	vm.messages = [];
	// show chat box if true
	vm.isShow = false;
	// get sender information from session
	vm.fromUser = JSON.parse(sessionStorage.user);
	// typing flag
	vm.isTyping = false;

	// send message function
	vm.send = (message)=>{
		// if message is not null
		if(message){
			vm.privateMessage = {
				from : vm.fromUser,
				to: vm.toUser,
				message: message,
				time: Date.now()
			};
			console.log('send private message: ', vm.privateMessage);
			// emit event to server
			socket.emit('send-private-message', vm.privateMessage);
			// add this message to message list
			vm.messages.push(vm.privateMessage);
			//clear textbox
			vm.message = '';

		}
	};

	// send message when Enter 
	vm.enter = (event, message)=>{
		// ENTER keycode = 13
		if(event.keyCode === 13){
			// call send message function
			vm.send(message);
		}
	};

	// emit event typing
	vm.typing = () => {
		console.log(vm.fromUser.username + " is typing...");
		// get sender and receiver information
		let data = {
			from : vm.fromUser,
			to: vm.toUser
		};
		// emit event
		socket.emit('typing', data);
	}

	// handle open-chat event from user-list component
	$rootScope.$on('open-chat', (event, toUser)=>{

		console.log('open-chat to: ',toUser);

		// set selected receiver
		vm.toUser = toUser;

		//get history messages between sender and receiver from server
		let url = '/messages/' + vm.fromUser.username + '/' + vm.toUser.username;

		$http.get(url).then(
			(response)=>{
				//success callback
				console.log('get messages: ', response);
				// set result to message list
				vm.messages = response.data;
				
			},
			(response)=>{
				//error callback
				console.log(response);
			});
		
		// display chat box
		vm.isShow = true;
		
	});

	// on receive message from other user
	socket.on('receive-private-message', (privateMessage)=>{
		console.log('received private message: ', privateMessage);

		// show message in chatbox when the sender of the message is the current receiver OR there is no current receiver
		if(!vm.toUser || vm.toUser.username == privateMessage.from.username){
			$scope.$apply(()=>{
				// open chat box
				vm.isShow = true;
				// add message to message list
				vm.messages.push(privateMessage);
				// set sender as current receiver
				vm.toUser = privateMessage.from;
			});
		}
	});

	// another user is typing
	socket.on('on-typing', (data) => {
		// if current receiver is typing
		if(data.from.username === vm.toUser.username) {
			$scope.$apply(()=>{
				// set typing flag
				vm.isTyping = true;
				$timeout( () => {
					// set false after 500 miliseconds
					vm.isTyping = false;
				}, 500);
			});
		}
		
	});

	
}