angular.
module('chatApp').
component('register', {
	templateUrl: '/client/register/register.template.html',
	controller: registerControler
});

function registerControler($http, $location) {

	let vm = this;

	// if user joined, redirect to chat page
	if(sessionStorage.user){
		$location.path('/chat/');
	}
	// check username in server side
	vm.checkUsername = (isGuest) => {
		let data = {username: vm.username};
		// set isGuest flag if user join as guest
		if(isGuest){
			data.isGuest = true;
		}
		$http.post("/checkUsername/", data).then(
			(response)=>{
					// success callback
					// clear error
					vm.usernameError = '';
					// store checked user to session
					sessionStorage.user = JSON.stringify(response.data.user);
					// redirect to /chat page
					$location.path('/chat/');
				},
				(response)=>{
					//error callback
					console.log('invalid username: ', response);
					// display error
					vm.usernameError = response.data.message;
				});
	};

	// check username when Enter 
	vm.enter = (event)=>{
		// ENTER keycode = 13
		if(event.keyCode === 13){
			// call check username function
			vm.checkUsername();
		}
	};
}

