let gravatar = require('gravatar');
let request = require('request');

/**
* Get gravatar function.
*
* @param {String} email
* @param {Function} callback
* 
*/
exports.getGravatar = function(username, fn){

	// get profile url by username
	let profile_url = gravatar.profile_url(username, {protocol: 'https'});
	console.log(profile_url);

	// set User-Agent to prevent 403 error
	let options = {
		url: profile_url,
		headers: {
			'User-Agent': 'request'
		}
	};

	// get profile for Gravatar
	request(options, (error, response, body) => {
		// if there is no error and response code is OK(200)
		if(!error && response && response.statusCode === 200){
			let gravatar = JSON.parse(body).entry[0];
			console.log('gravatar: ', gravatar); 
			//return gravatar
			return fn(null, gravatar);
		}
		console.log('error:', error); 
		console.log('statusCode:', response && response.statusCode);
		// return error
		return fn(new Error(error));
});
}