let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
let path = require('path');
let bodyParser = require('body-parser');

// service that get gravatar profile
let myGravatar = require('./gravatar/gravatar.service.js');

// default port
const PORT = 3000;

// default guest name
const GUEST_NAME = "guest";

// list of connecting users
let users = {};

// guest counter
let guestCount = 0;

// list of messages
let messages = [];

app.use('/client', express.static(path.join(__dirname, '../client')));
app.use('/node_modules', express.static(path.join(__dirname, '../node_modules')));
app.use(bodyParser.json());


/**
 * check message's sender and receiver
 */
function isMessageOf(message, from, to) {
	return ((message.from.username === from && message.to.username === to) ||
		(message.from.username === to && message.to.username === from));
}

/**
 * check valid email
 */
function isEmail(username){
	const EMAIL_REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
	return EMAIL_REGEX.test(username);
}


/**
 * ROUTING
 */
 // get index page
app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, '../client/', 'index.html'))
});

// get all connected users
app.get('/users', (req, res) => {
	return res.send({users: users});
});

// get messages between 2 users: fromUser and toUser
app.get('/messages/:from/:to', (req, res) => {
	//get messages
	let result = messages.filter((message) => {
		return isMessageOf(message, req.params.from, req.params.to);
	});
	console.log(result);
	return res.send(result);
});

// check username
app.post('/checkUsername', (req, res) => {
	let user = req.body;

	user.hasGravatar = false;

	// if join as guest, return random name
	if(user.isGuest){
		guestCount+=1;
		user.username = GUEST_NAME + '_' + guestCount;
		return res.send({user: user});
	}

	// if username is blank
	if(!user.username){
		return res.status(400).send({message: 'Username is required!'});
	}

	// if username is duplicated
	if(users[user.username]){
		return res.status(400).send({message: 'Username is taken! Please choose another name!'});
	}

	// if username is email
	if(isEmail(user.username)){
		// get user profile from Gravatar
		myGravatar.getGravatar(user.username, (error, gravatar) => {
			if(error){
				console.log('error: ', error);
				return res.send({user: user});
			}
			console.log('set gravatar to user: ', gravatar);
			user.gravatar = gravatar;
			user.hasGravatar = true;
			return res.send({user: user});
		});
	} else {
		return res.send({user: user});
	}


});


/**
 * SOCKET.IO EVENTS
 */
 // new socket connected
io.on('connection', (socket) => {

	console.log('socket connected:', socket.id );

	// register new user
	socket.on('new-user', (user) => {
		// add new user to user list
		users[user.username] = user;
		users[user.username].id = socket.id;
		socket.username = user.username;
		console.log('new user:', users[user.username]);
		console.log('add user list ', users);
		// emit add-user-list to add this user to user list
		socket.broadcast.emit('add-user-list', users[user.username]);
	});

	// send private message to an user
	socket.on('send-private-message', (privateMessage) => {
		console.log('send private message: ', privateMessage);
		// add message to message list
		messages.push(privateMessage);
		// emit an event to the receiver
		socket.to(privateMessage.to.id).emit('receive-private-message', privateMessage);
	});

	// typing event
	socket.on('typing', (data) => {
		console.log('typing: ', data);
		// emit an event to the receiver
		socket.to(data.to.id).emit('on-typing', data);
	});

	// socket disconnect
	socket.on('disconnect', () => {
		// remove this user from user list
		delete users[socket.username];
		console.log('disconnected ', socket.username);
		// emit an event to remove this user from user list in client side
		socket.broadcast.emit('remove-user-list', socket.username);
	});
});

// server listening on PORT
http.listen(PORT, () => {
	console.log('listening on *:' + PORT);
});



